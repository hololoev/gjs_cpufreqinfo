#!/usr/bin/gjs

const Gtk = imports.gi.Gtk;
const GLib  = imports.gi.GLib;
const GObj  = imports.gi.GObject;

imports.searchPath.push('./');

const chart = imports.modules.chart;
const getBody = imports.modules.getBody.getBody;
const setInterval = imports.modules.timers.setInterval;
const getCPUInfo = imports.modules.cpuinfo.getCPUInfo;
const updateCells = imports.modules.getBody.updateCells;

let app = new Gtk.Application({ application_id: 'org.gtk.advancedHello' });
let chartStore = [];

app.connect('activate', () => {
  log('App started');
  
  let mainWin = new Gtk.ApplicationWindow({
    application: app,
    window_position: Gtk.WindowPosition.CENTER,
    title: 'Advanced Hello World on gjs'
  });
  let container = new Gtk.Box({});
  
  let leftFrame = new Gtk.Frame({});
  let rightFrame = new Gtk.Frame({});
  let drawArea = new Gtk.DrawingArea();
  
  drawArea.connect('draw', chart.cleanSurface);
  leftFrame.add(drawArea);
  
  container.add(leftFrame);
  container.add(rightFrame);
  
  leftFrame.set_size_request(500, 300);  
  //rightFrame.set_size_request(250, 300);
  
  let cpuInfo = getCPUInfo();
  rightFrame.add(getBody(this, cpuInfo));
  
  mainWin.add(container);
  mainWin.show_all();
  
  let updateInterval = setInterval( () => {
    let cpuInfo = getCPUInfo();
    chartStore.push(cpuInfo);
    updateCells(this, cpuInfo);
    chart.drawChart(drawArea, leftFrame, chartStore, cpuInfo.length);
  }, 1000);
});

app.run([]);
