
const GLib  = imports.gi.GLib;

const procPath = '/proc/cpuinfo';

function parseCPUInfo(text) {
  let result = [];
  let lines = text.toString().split('\n');  
  
  let curProcessor = 0;
  for(let line of lines) {
    
    let parts = line.split(':');
    if( parts.length <= 1 )
      continue;
    
    let key = parts[ 0 ].trim();
    let val = parts[ 1 ].trim();
    
    if( key === 'processor' )
      curProcessor = parseInt(val);
    
    if( key == 'cpu MHz' )
      val = parseInt(val);
    
    if( !result[ curProcessor ] )
      result[ curProcessor ] = {};
    
    result[ curProcessor ][ key ] = val;
  }
  
  return result;
}

function getCPUInfo() {
  let [ok, contents] = GLib.file_get_contents(procPath);
  return parseCPUInfo(contents);
}
