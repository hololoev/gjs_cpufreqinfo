
const Gtk = imports.gi.Gtk;
const GObj  = imports.gi.GObject;

function cellFuncText1(col, cell, model, iter) {
  cell.editable = false;
  cell.text = model.get_value(iter, 1);
};

function cellFuncText2(col, cell, model, iter) {
  cell.editable = false;
  cell.text = model.get_value(iter, 2);
};

function updateCells(parent, cpuInfo) {
  parent.store.foreach(function(model, box, iter) {
    let index = model.get_value(iter, 0);
    parent.store.set(iter, [1, 2], [`C${cpuInfo[index].processor}`, cpuInfo[index]['cpu MHz']]);
  });
}

function getBody(parent, cpuInfo) {
  parent.scroll = new Gtk.ScrolledWindow({ vexpand: true });
  parent.scroll.set_size_request(250, 300);
  
  parent.store = new Gtk.ListStore();
  parent.store.set_column_types([GObj.TYPE_INT, GObj.TYPE_STRING, GObj.TYPE_STRING, GObj.TYPE_BOOLEAN]);
  
  for(let index=0; index<cpuInfo.length; index++)
    parent.store.set(parent.store.append(), [0, 1, 2, 3], [index, `C${cpuInfo[index].processor}`, cpuInfo[index]['cpu MHz'], false]);

  parent.tree = new Gtk.TreeView({ headers_visible: true, vexpand: true, hexpand: true });
  parent.tree.set_model(parent.store);  
  parent.scroll.add(parent.tree);

  parent.col = new Gtk.TreeViewColumn({ title: 'CPU' });
  parent.col2 = new Gtk.TreeViewColumn({ title: 'MHz' });
  parent.tree.append_column(parent.col);
  parent.tree.append_column(parent.col2);

  parent.text1 = new Gtk.CellRendererText();
  parent.col.pack_start(parent.text1, true);
  parent.col.set_cell_data_func(parent.text1, (col, cell, model, iter) => { cellFuncText1(col, cell, model, iter, cpuInfo); });

  parent.text2 = new Gtk.CellRendererText();
  parent.col2.pack_start(parent.text2, true);
  parent.col2.set_cell_data_func(parent.text2, (col, cell, model, iter) => { cellFuncText2(col, cell, model, iter, cpuInfo); });

  return parent.scroll;
};
