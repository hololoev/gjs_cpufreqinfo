
const Gdk = imports.gi.Gdk;
const cairo = imports.cairo;

const colors = [
  [190, 0, 0, 0.5],
  [0, 190, 0, 0.5],
  [0, 0, 190, 0.5],
  [190, 0, 190, 0.5],
  [190, 190, 0, 0.5],
  [0, 190, 190, 0.5],
  [255, 0, 0, 0.5],
  [0, 255, 0, 0.5],
  [0, 0, 255, 0.5],
  [128, 0, 255, 0.5],
  [0, 128, 255, 0.5],
  [255, 128, 0, 0.5],
  [255, 0, 128, 0.5],
  [0, 255, 128, 0.5],
  [128, 255, 0, 0.5],
  [128, 255, 128, 0.5],
];

function cleanSurface(drawArea) {
  let surface = drawArea.get_window();
  let context = Gdk.cairo_create(surface);
  
  context.operator = cairo.Operator.CLEAR;
  context.paint();
  context.operator = cairo.Operator.OVER;
}

function getMaxMinVal(chartStore, maxWidth) {
  let maxVal = 0;
  let minVal = 9999999;
  
  for(let index=(chartStore.length-1); (index>=0 && (index>=(chartStore.length-maxWidth))); index--) {
    for(let cpuIndex=0; cpuIndex<chartStore[index].length; cpuIndex++) {
      if( chartStore[ index ][ cpuIndex ][ 'cpu MHz' ] > maxVal )
        maxVal = chartStore[ index ][ cpuIndex ][ 'cpu MHz' ];
      
      if( chartStore[ index ][ cpuIndex ][ 'cpu MHz' ] < minVal )
        minVal = chartStore[ index ][ cpuIndex ][ 'cpu MHz' ];
    }
  }
  
  return [ maxVal, minVal ];
}

function calcHeight(val, maxVal, minVal, surfaceHeight) {
  let realLength = val - minVal;
  let realMaxLength = maxVal - minVal;
  
  return surfaceHeight - parseInt( (realLength/realMaxLength) * surfaceHeight );
}

function drawChart(drawArea, parentFrame, chartStore, cpuCount) {
  
  if( chartStore.length <2 )
    return;
  
  let surface = drawArea.get_window();
  let context = Gdk.cairo_create(surface);
  let [ width, height ] = parentFrame.get_size_request();
  
  context.operator = cairo.Operator.CLEAR;
  context.paint();
  context.operator = cairo.Operator.OVER;
  
  let [ maxVal, minVal ] = getMaxMinVal(chartStore, width);  
  context.setLineWidth (1);
  
  let colorIndex = 0;
  for(let cpuIndex=0; cpuIndex<cpuCount; cpuIndex++) {
    context.setSourceRGBA (...colors[ colorIndex ]);
    
    let startVal = chartStore[ chartStore.length-1 ][ cpuIndex ]['cpu MHz'];
    context.moveTo(width - 1, calcHeight(startVal, maxVal, minVal, height));
    
    let xOffset = 1;
    for(let index=chartStore.length-1; ( (index>=0) && (index>(chartStore.length-width)) ); index--) {
      let val = chartStore[ index ][ cpuIndex ]['cpu MHz'];
      let pHeight = calcHeight(val, maxVal, minVal, height);
      context.lineTo(width - xOffset, pHeight );
      xOffset ++;
    }
    context.stroke();
    
    colorIndex ++;
    if( colorIndex >= colors.length )
      colorIndex = 0;
  }
  
  return false;
} 
